/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pakorn.oxprogramoop;

/**
 *
 * @author Kirito
 */
public class Table {
   private char[][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
   private Player playerX;
   private Player playerO;
   private Player currentPlayer;
   private Player winner;
   private boolean finish = false;
   private int lastcol;
   private int lastrow;
    public Table(Player x,Player o){
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }
    public void ShowTable(){
        System.out.println("1 2 3");
        for(int i=0; i<table.length;i++){
            System.out.print(i+" ");
            for(int j=0; j<table[i].length;j++){
                System.out.print(table[i][j]);
            }
            System.out.println(" ");
        }
    }
    public boolean setRowCol(int row,int col){
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
            }
            return false;
    }
    public Player getcurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
        }else {
            currentPlayer = playerX;
        }
    }
        void checkCol(){
        for(int row=0; row<3; row++){
            if(table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        SetstatwinLose();
    }
      void checkRow(){
        for(int col=0; col<3; col++){
            if(table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner =currentPlayer;
        SetstatwinLose();
    }

    private void SetstatwinLose() {
        if(currentPlayer==playerO){
            playerO.win();
            playerX.lose();
        }else {
            playerO.lose();
            playerX.win();
        }
    }
      void checkX1() {
        for(int i=0; i<3; i++){
            if(table[i][i] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        SetstatwinLose();
    }
       void checkX2() {
        for(int i=0; i<3; i++){
            if(table[2-1][i] != currentPlayer.getName()){
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        SetstatwinLose();
    }
      void checkDraw() {
         for(int row=0; row< 3 ; row++){
            for(int col=0; col< 3 ; col++){
                if(table[row][col] == '-'){
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
            playerO.draw();
     }
    public void checkWin() {
        checkRow();
        checkCol();
        checkX1();
        checkX2();
        checkDraw();
    }
     public boolean isFinish(){
         return finish;
     }
     public Player getWinner(){
         return winner;
     }
}
